import java.util.Arrays;
import java.util.Scanner;

public class Seat extends Detail {
    private String[][] seat1 = { { "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10" },
            { "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "B10" },
            { "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10" },
            { "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "D10" } };
    private static String inputSeat;
    private static String inputTicket;
    private String[] selectedSeat;
    private int count = -1;

    Seat() {
        System.out.println("\n===========[ Select Seats ]=============");
        System.out.println("\n----------- Avalilable Seat ------------");
        System.out.print("             [ Screen ]             \n");
        for (int i = 0; i < seat1.length; i++) {
            System.out.print("   ");
            for (int j = 0; j < seat1[i].length; j++) {
                System.out.print(seat1[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void selectedSeat() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\nPlease input number of ticket: ");
        int ticket = 0;
        while (true) {
            try {
                int inputNumberTicket = sc.nextInt();
                while (true) {
                    if (inputNumberTicket > 40) {
                        System.out.println("Your number ticke out of Limit! (Have 40 Seat)");
                        System.out.print("Please input again! : ");
                        inputNumberTicket = sc.nextInt();

                    } else {
                        break;
                    }
                }

                ticket = inputNumberTicket;
                break;
            } catch (java.util.InputMismatchException e) {
                System.out.println("Input Only Integer.");
                System.out.print("Please input number of ticket: ");
                sc.next();
            }
        }

        setTicket(ticket);
        selectedSeat = new String[ticket];

        for (int w = 0; w < ticket; w++) {
            boolean validSeat = false;
            boolean invalidSeat = false;

            while (!validSeat) {
                if (!invalidSeat) {
                    System.out.print("Please input number of Seat: ");
                } else {
                    System.out.print("Invalid seat. Please choose a valid seat: ");
                    System.out.println();
                }

                inputSeat = sc.next().toUpperCase();

                // Validate if the input seat is valid and available
                boolean seatFound = false;
                for (int i = 0; i < seat1.length; i++) {
                    for (int j = 0; j < seat1[i].length; j++) {
                        if (inputSeat.equals(seat1[i][j])) {
                            seatFound = true;
                            if (seat1[i][j].equals("XX")) {
                                invalidSeat = true;
                                break; // Exit the loop if the seat is already taken
                            } else {
                                seat1[i][j] = "XX";
                                count++;
                                selectedSeat[count] = inputSeat;
                                validSeat = true; // Valid seat selected
                            }
                        }
                        else{
                            seatFound = false;
                        }
                    }
                    if (seatFound) {
                        break;
                    }
                }

                if (!seatFound) {
                    invalidSeat = true;
                }
            }
        }

        showSeat();
        setSeat(selectedSeat);
    }

    public void showSeat() {
        System.out.println("\n----------- Avalilable Seats -----------");
        System.out.print("              [ Screen ]              \n");
        for (int i = 0; i < seat1.length; i++) {
            System.out.print("   ");
            for (int j = 0; j < seat1[i].length; j++) {
                System.out.print(seat1[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void getSeat() {
        for (int i = 0; i < selectedSeat.length; i++) {
            System.out.print(selectedSeat[i] + " ");
        }
    }

}
