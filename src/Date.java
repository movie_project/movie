import java.util.Scanner;

public class Date extends Detail{
    private String[] date = {"11/01/2011","12/01/2011","13/01/2011"};
    private String selectedDate;
    private String setDate;

    Date(){
        System.out.println("\n==============[ Showtime ]==============");
        System.out.println("------------- Select Date --------------");
        System.out.println("Date List : ");
        for(int i=0;i<date.length;i++){
            System.out.println("   "+"("+(i+1)+") "+date[i]);
        }
    }

    public String inputDate(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select Date : ");
        String inputDate = sc.next();
        if (inputDate.matches("[1-3]")) {
            selectedDate = inputDate;
        }else{
            System.out.print("Please Enter Correct Date : ");
            inputDate = sc.next();
            selectedDate = inputDate;
        }
        return selectedDate;
    }

    public void selectedDate(){
        selectedDate = inputDate();
        if(selectedDate.equals("1")){
            setDate = date[0];
            setDate(setDate);
        }else if(selectedDate.equals("2")){
            setDate = date[1];
            setDate(setDate);
        }else if(selectedDate.equals("3")){
            setDate = date[2];
            setDate(setDate);
        }else{
            System.out.print("Date Not Found:\n");
            selectedDate();
        }
    }

    public String getDate(){
        return setDate;
    }
}
