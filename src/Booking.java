public class Booking {
    public static void main(String[] args) {
        System.out.println("\n======[ Welcome to Booking System ]======\n");
        boolean stop = true;

        while (stop) {
            Movie m = new Movie();
            m.selectedMovie();
            System.out.println("Selected Movie : " + m.getMovie());

            Subtitles sub = new Subtitles();
            sub.selectedSub();
            System.out.println("Selected Subtitles : " + sub.getSub());

            Date d = new Date();
            d.selectedDate();
            System.out.println("Selected Date : " + d.getDate());

            Time t = new Time();
            t.selectedTime();
            System.out.println("Selected Time : " + t.getTime());

            Seat s = new Seat();
            s.selectedSeat();
            System.out.print("\nSelected Seat : ");
            s.getSeat();

            Detail detail = new Detail();
            detail.showDetail();

            Confirm c = new Confirm();
            stop = c.Confirm();
        }

    }
}
