import java.util.Scanner;

public class Payment extends Detail {
    private static int cash;
    private static int price;

    public void Payment() {
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.println("\n------------ Payment System ------------");
            System.out.println("Total Amount "+Price()+" Baht");
            System.out.print("Enter Payment Amount : ");
            price = Price();
            
            try {
                cash = sc.nextInt();
            } catch (java.util.InputMismatchException e) {
                System.out.println("Invalid input. Please enter a valid integer.");
                sc.next();
                continue;
            }
            price = Price();
            if (cash >= price) {
                cash = cash - price;
                System.out.println("Change : " + cash + " Baht");
                break;
            } else {
                System.out.println("!!! Money not enough !!!");
            }
        }
    }
}
