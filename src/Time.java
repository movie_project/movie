import java.util.Scanner;

public class Time extends Detail{
    private String[] time = {"11:00","12:00","13:00"};
    private String selectedTime;
    private String setTime;

    Time(){
        System.out.println("------------- Select Time --------------");
        System.out.println("Time List : ");
        for(int i=0;i<time.length;i++){
            System.out.println("   "+"("+(i+1)+") "+time[i]);
        }
    }

    public String inputTime(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select Time : ");
        String inputTime = sc.next();
        if (inputTime.matches("[1-3]")) {
            selectedTime = inputTime;
        }else{
            System.out.print("Please Enter Correct Time : ");
            inputTime = sc.next();
            selectedTime = inputTime;
        }
        return selectedTime;
    }

    public void selectedTime(){
        selectedTime = inputTime();
        if(selectedTime.equals("1")){
            setTime = time[0];
            setTime(setTime);
        }else if(selectedTime.equals("2")){
            setTime = time[1];
            setTime(setTime);
        }else if(selectedTime.equals("3")){
            setTime = time[2];
            setTime(setTime);
        }
        else{
            System.out.print("Time Not Found:\n");
            selectedTime();
        }
    }

    public String getTime(){
        return selectedTime;
    }
}