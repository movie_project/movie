public class Detail{
    private static String nameMovie;
    private static String[] seat;
    private static String showTime;
    private static String showDate;
    private static String showSub;
    private static int showTicket;

    public void showDetail() {
        System.out.println("\n\n----------- Booking Detail -------------");
        System.out.println("Movie : "+setMovieName(nameMovie));
        System.out.println("Subtitles : "+setSub(showSub));
        System.out.println("Show Date : "+setDate(showDate));
        System.out.println("Show Time : "+setTime(showTime));
        System.out.println("Number of tickets : "+setTicket(showTicket));
        System.out.print("Selected Seat : ");
        printSeat();
        System.out.println("\nTotal Price : " +Price()+" Baht");
        System.out.println("----------------------------------------");
        
    }

    public String setMovieName(String selectedMovie){
        nameMovie = selectedMovie;
        return nameMovie;
    }

    public String setSub(String selectedSub){
        showSub = selectedSub;
        return showSub;
    }

    public String setTime(String selectedTime){
        showTime = selectedTime;
        return showTime;
    }

    public String setDate(String selectedDate){
        showDate  = selectedDate;
        return showDate;
    }

    public int setTicket(int ticket){
        showTicket = ticket;
        return showTicket;
    }

    public int Price(){
        int x = setTicket(showTicket);
        x = x*150;
        return x;
    }
    
    public void setSeat(String[] selectedSeat){
        seat = selectedSeat;
    }

    public void printSeat(){
        for(int i=0;i<seat.length;i++){
            System.out.print(seat[i]+" ");
        }
    }
}
