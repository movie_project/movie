import java.util.Scanner;

public class Confirm extends Payment {
    private static String ans;

    public boolean Confirm() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Confirm Booking? (Y/N) : ");
        ans = sc.next().toLowerCase();

        while (true) {
            try {
                if (ans.equals("y")) {
                    Payment();
                    System.out.println("----------------------------------------");
                    System.out.println("Booking Success!");
                    System.out.println("Thank you for using the service.");
                    System.out.println("----------------------------------------");
                    return false;
                } else if (ans.equals("n")) {
                    return true;
                } else {
                    System.out.print("Please input only Y/y/N/n : ");
                    ans = sc.next().toLowerCase();
                }
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
            }
        }
    }
}
