import java.util.Scanner;

public class Movie extends Detail{
    private String[] movie = {"Mickey Mouse","Minions","Ariel"};
    private String selectedMovie;
    private String setMovie;

    Movie(){
        System.out.println("---------- Now Showing Movies ----------");
        System.out.println("Movie List : ");
        for(int i=0;i<movie.length;i++){
            System.out.println("   "+"("+(i+1)+") "+movie[i]);
        }
    }

    public String inputMovie(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select Movie : ");
        String inputMovie = sc.next();
        if (inputMovie.matches("[1-3]")) {
            selectedMovie = inputMovie;
        }else{
            System.out.print("Please Enter Correct Movie : ");
            inputMovie = sc.next();
            selectedMovie = inputMovie;
        }
        return selectedMovie;
    }

    public void selectedMovie(){
        selectedMovie = inputMovie();
        if(selectedMovie.equals("1")){
            setMovie = movie[0];
            setMovieName(setMovie);
        }else if(selectedMovie.equals("2")){
            setMovie = movie[1];
            setMovieName(setMovie);
        }else if(selectedMovie.equals("3")){
            setMovie = movie[2];
            setMovieName(setMovie);
        }else{
            System.out.print("Movie Not Found:\n");
            selectedMovie();
        }
    }

    public String getMovie(){
        return setMovie;
    }
}
