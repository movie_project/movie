import java.util.Scanner;

public class Subtitles extends Detail{
    private String[] sub = {"Thai Subtitles","Eng Subtitles"};
    private String selectedSub;
    private String setSub;

    Subtitles(){
        System.out.println("---------- Select Subtitles ------------");
        for(int i=0;i<sub.length;i++){
            System.out.println("   "+"("+(i+1)+") "+sub[i]);
        }
    }

    public String inputSub(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select Subtitle : ");
        String inputSub = sc.next();
        if (inputSub.matches("[1-2]")) {
            selectedSub = inputSub;
        }else{
            System.out.print("Please Enter Correct Subtitle : ");
            inputSub = sc.next();
            selectedSub = inputSub;
        }
        return selectedSub;
    }

    public void selectedSub(){
        selectedSub = inputSub();
        if(selectedSub.equals("1")){
            setSub = sub[0];
            setSub(setSub);
        }else if(selectedSub.equals("2")){
            setSub = sub[1];
            setSub(setSub);
        }else{
            System.out.print("Subtitle Not Found:\n");
            selectedSub();
        }
    }

    public String getSub(){
        return setSub;
    }
}
